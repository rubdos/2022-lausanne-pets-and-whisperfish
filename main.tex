\documentclass[aspectratio=169]{beamer}
\usetheme[coloredtitles,coloredblocks]{vub} % This will automatically load all fonts (Roboto and TeX Gyre Adventor
               % for titles), and the VUB colors. Also includes the triangle.

% The \usetheme{vub} command accepts many different options:
% - coloredtitles (background of the title on a slide in VUB style)
% - coloredblocks (colours for theorems etc.)
% - showsection (display the current \section on every slide)

% The `dept` option is special, and replaces the VUB logo with a merged
% VUB-department logo.
% Currently supported:
% - dept=etro
% - dept=soft
% - dept=ai
% - dept=brubotics
% If you miss a department logo, file an issue at
% https://gitlab.com/rubdos/texlive-vub/issues

% Read the full documentation at
% https://gitlab.com/rubdos/texlive-vub

\usepackage{tikzpeople}
\usetikzlibrary{positioning,shapes, arrows.meta, calc, tikzmark}

\usepackage{multicol}

\title{Sailfish OS and Signal}
\subtitle{Your Phone \& You @ FIXME} % Can be omitted
\author{Ruben De Smet}
\date{October 22, 2022}

\begin{document}
\frame{\maketitle} % Automatic titlepage with VUB logo.

% 1. End-to-end encryption
% 2. Client-server

\begin{frame}{How to talk to people}
  \framesubtitle{off the record}
  \begin{columns}
    \begin{column}{.5\linewidth}
      \begin{center}
        \begin{tikzpicture}
          \node[alice,minimum size=1.5cm] (alice) {Alice};
          \node[bob,mirrored,minimum size=1.5cm] (bob) at (4, 0) {Bob};

          % Scenario 1: direct communication.
          \draw<2>[thick,->] (alice) -- node[above] {``Hello!''} (bob);

          % Scenario 2: plain text communication
          \onslide<3->{\node (phone-alice) at (1, 2) {\includegraphics[height=1cm]{phone.png}};}
          \onslide<3->{\node (phone-bob) at (3, 2) {\includegraphics[height=1cm]{phone.png}};}
          \draw<4->[thick,->] (alice) |- node[above] {\footnotesize ``Hello!''} (phone-alice);
          \draw<4->[thick,->] (phone-bob) -| node[above] {\footnotesize ``Hello!''} (bob);
          \draw<4->[thick,->] (phone-alice) -- node[above] {\footnotesize ``Hello!''} (phone-bob);

          \onslide<5->{\draw[dashed,red,very thick] (1, 3) rectangle (3, 1);}
        \end{tikzpicture}
      \end{center}
    \end{column}
    \begin{column}{.5\linewidth}
      \begin{itemize}
        \item Alice and Bob like to \emph{talk}.
        \item<2-> In real life, it's easy. \emph{Just shout}.
        \item<3-> Bob's far away? Use \emph{the phone}!
        \item<5-> What could possibly \tikz[baseline=-1mm]{\node[draw=red,dashed,very thick]{go wrong}}?
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{End-to-end encryption}
  \framesubtitle{the baseline for privacy}
  \begin{center}
    \begin{tikzpicture}
      \node[alice,minimum size=1.5cm] (alice) {};
      \node[bob,mirrored,minimum size=1.5cm] (bob) at (8, 0) {};

      \onslide{\node (phone-alice) at (1, 2) {\includegraphics[height=1cm]{phone.png}};}
      \onslide{\node (phone-bob) at (7, 2) {\includegraphics[height=1cm]{phone.png}};}
      \draw[thick,->] (alice) |- node[above] {\footnotesize ``Hello!''} (phone-alice);
      \draw[thick,->] (phone-bob) -| node[above] {\footnotesize ``Hello!''} (bob);
      \draw<1,2>[thick,->] (phone-alice) -- node[above] {\footnotesize \only<1>{``Hello!''} \only<2>{\texttt{0x1213DC10}}} (phone-bob);

      \onslide<1>{\draw[dashed,red,very thick] (1, 3) rectangle (7, 1);}

      \onslide<3->{\node[cloud,draw,fill=gray!5,aspect=1.61] (cloud) at (4, 2) {\footnotesize The Cloud};}
      \draw<3->[thick,->] (phone-alice) -- node[above] {\footnotesize \texttt{0x1213..}} (cloud);
      \draw<3->[thick,->] (cloud) -- node[above] {\footnotesize \texttt{..DC10}} (phone-bob);
    \end{tikzpicture}

    \bigskip

    \onslide<4->{
      What happens in \emph{The Cloud},
      stays in \emph{The Cloud}.
    }
  \end{center}
\end{frame}

\begin{frame}{End-to-end encryption}
  \framesubtitle{the baseline for privacy}

  Some properties of end-to-end encryption:
  \begin{description}
    \item[Confidentiality] The Cloud cannot \emph{read} the messages
    \item[Forward secrecy] Compromised \emph{key material} does not imply \emph{compromised sessions}
  \end{description}

  \pause
  \bigskip

  What about \emph{metadata}?
  \begin{itemize}
    \item When do Alice and Bob talk?
    \item From where are the messages sent?
    \item Group structures?
  \end{itemize}

  \pause
  \bigskip

  \begin{center}
    \large
    E2EE is a \emph{minimum necessary requirement} for IM.
  \end{center}
\end{frame}

\begin{frame}{Signal}
  \begin{columns}
    \begin{column}{.6\linewidth}
      Pioneers in secure messaging.
      Well known:
      \begin{itemize}
        \item End-to-end encryption\footnotemark
        \item Easy to use
      \end{itemize}
      \bigskip
      \onslide<2> {
        Signal differs a lot from ``competitors''.
      }
    \end{column}
    \begin{column}{.4\linewidth}
      \includegraphics[width=\linewidth]{signal.png}
    \end{column}
  \end{columns}
  \footnotetext{WhatsApp uses \texttt{libsignal-protocol}}
\end{frame}

\begin{frame}{Minimizing metadata (1)}
  \framesubtitle{Sealed sending\footnotemark}
  \footnotetext{Sealed sending on Signal's blog (2018): \url{https://signal.org/blog/sealed-sender/}}

  \begin{columns}
    \begin{column}{.65\linewidth}
      \begin{tikzpicture}
        \node[alice,minimum size=1.5cm] (alice) {};
        \node[bob,mirrored,minimum size=1.5cm] (bob) at (7, 0) {};

        \onslide{\node (phone-alice) at (1, 2) {\includegraphics[height=1cm]{phone.png}};}
        \onslide{\node (phone-bob) at (6, 2) {\includegraphics[height=1cm]{phone.png}};}
        \draw[thick,->] (alice) |- node[above] {\footnotesize ``Hello!''} (phone-alice);
        \draw<3,6->[thick,->] (phone-bob) -| node[above] {\footnotesize ``Hello!''} (bob);

        \onslide{\node[cloud,draw,fill=gray!5,aspect=1.61] (cloud) at (3.5, 2) {\footnotesize The Cloud};}

        \draw<2-3,5->[thick,->] (phone-alice) -- node[above,draw,vuboranje,circle] {1} (cloud);
        \draw<3,6->[thick,->] (cloud) -- node[above,draw,vuboranje,circle] {2} (phone-bob);
      \end{tikzpicture}
    \end{column}
    \begin{column}{.35\linewidth}
      Alice sends a \only<4->{\emph{sealed}} \emph{message} to Bob via The Cloud.
      \only<1-3> {
        \begin{enumerate}
          \item \onslide<2->{``\emph{I'm Alice}! Please forward \texttt{0x1213DC10}''}
          \item \onslide<3->{``Hi Bob! Here's \emph{Alice}'s message: \texttt{0x1213DC10}''}
        \end{enumerate}
      }
      \only<5-> {
        \begin{enumerate}
          \item \onslide<5->{``\emph{I know Bob}! Please forward \texttt{0x1213DC10}''}
          \item \onslide<6->{``Hi Bob! Here's \emph{someone}'s message: \texttt{0x1213DC10}''}
        \end{enumerate}
      }
    \end{column}
  \end{columns}

  \bigskip
  \begin{center}
    \onslide<7->{Sealed sending hides the sender of messages from \emph{The Cloud}.}
  \end{center}
\end{frame}

\begin{frame}{Authentication server}
  Classic solution: application \emph{asks The Cloud} for credentials.

  \begin{center}
    \begin{tikzpicture}
      \node[alice, minimum size=1.5cm] (peggy) at (0, 0) {};
      \node at (-1.2, -.4) {\includegraphics[width=3mm]{phone.png}};
      \node[businessman,mirrored, minimum size=1.5cm] (grace) at (6, 1.5) {Grace};
      \node[guard,mirrored, minimum size=1.5cm] (victor) at (6, -1.8) {Victor};
      \node at (7.6, -2.0) {\includegraphics[width=10mm]{db.png}};
      %
      \draw[->] (.7, 1.8) -- node[above] {1.\; Request credential} (5, 1.8);
      \draw[<-] (.7, 1.2) -- node[below] {2.\; Receive credential} (5, 1.2);
      %
      \draw[->] (.7, -1.5) -- node[above] {3.\; Show \emph{credential}} (5, -1.5);
      \draw[<-] (.7, -2.1) -- node[below] {4.\; Grant permission} (5, -2.1);
    \end{tikzpicture}
  \end{center}

  \bigskip

  Problem: credential is still \emph{traceable}, because Grace == Victor.
\end{frame}

\begin{frame}{Zero-knowledge proof of Knowledge}
  Alice \emph{autonomously proves} her privileges.

  \begin{center}
    \begin{tikzpicture}
      \node[alice, minimum size=1.5cm] (peggy) at (0, 0) {};
      \node at (-1.2, -.4) {\includegraphics[width=3mm]{phone.png}};
      \node[businessman,mirrored, minimum size=1.5cm] (grace) at (6, 1.5) {Grace};
      \node[guard,mirrored, minimum size=1.5cm] (victor) at (6, -1.8) {Victor};
      \node at (7.6, -2.0) {\includegraphics[width=10mm]{db.png}};
      %
      \draw[->,gray] (.7, 1.8) -- node[above] {\color{gray}{1.\; Request credential}} (5, 1.8);
      \draw[<-,gray] (.7, 1.2) -- node[below] {\color{gray}{2.\; Receive credential}} (5, 1.2);
      %
      \draw[->] (.7, -1.5) -- node[above] {1.\; Show \emph{proof}} (5, -1.5);
      \draw[<-] (.7, -2.1) -- node[below] {2.\; Grant permission} (5, -2.1);
      % Cross
      \draw[red,very thick] (1, 0.5) -- (7, 2.5);
      \draw[red,very thick] (1, 2.5) -- (7, 0.5);
    \end{tikzpicture}
  \end{center}

  \bigskip

  Problem: the proof is \emph{random data} for Victor, an \emph{anonymous credential}.
\end{frame}

\begin{frame}{Zero-knowledge proof of Knowledge}
  \begin{center}
    How to \emph{minimize}\footnotemark{} disclosed data?
    \footnotetext{GDPR: ``Personal data shall be adequate, relevant and limited to what is necessary in relation to the purposes for which they are processed (‘data minimisation’);''}
  \end{center}

  \bigskip

  \begin{center}
    \begin{tikzpicture}
      \node<1-> (eid) at (0,0) {\includegraphics[width=3cm]{id.png}};
      \node<2-> (gehaktmolen) at (5,0) {\includegraphics[width=4cm]{meat-grinder.png}};
      \node<2-> (bits) at (10,0) {\includegraphics[width=3cm]{encrypted-blob.png}};
      \draw<2->[thick,->] (eid) -- (gehaktmolen);
      \draw<2->[thick,->] (gehaktmolen) -- (bits);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Zero-knowledge proof of Knowledge}

  ``I, Alice, prove that I have \emph{knowledge} that:
  \begin{itemize}
    \item my personal data allows me to drink beer
    \item my personal data allows me to vote
    \item my credentials allow me to access these parts of the database
  \end{itemize}
  \dots\ \emph{without revealing} my personal data.''
\end{frame}

\begin{frame}{Minimizing metadata (2)}
  \framesubtitle{Group structure}

  \begin{center}
    \begin{tikzpicture}
      \node[alice,minimum size=8mm] (alice) at (0, 0) {};
      \node[bob,minimum size=8mm] (bob) at (1, 0) {};
      \node[charlie,minimum size=8mm] (charlie) at (2, 0) {};
      \node[dave,minimum size=8mm] (dave) at (3, 0) {};
      \draw[vubbleu,dashed,very thick,rounded corners] (-1, -.7) rectangle (3.5, 0.7);
      \draw[vubbleu,dotted,thick,rounded corners] (-.5, -.7) -- (-.5, 0.7);
      \node[vuboranje,rotate=90] at (-.75, 0) {\small\textsc{\textbf{Family}}};
    \end{tikzpicture}
  \end{center}

  \pause

  \begin{columns}[t]
    \begin{column}{.5\linewidth}
      \textbf{Server fan-out}
        \begin{tikzpicture}
          \node[alice,minimum size=1cm] (alice) at (0, 0) {};
          \node at (-1, -.4) {\includegraphics[width=3mm]{phone.png}};
          \node[guard,mirrored, minimum size=1cm] (server) at (2, 0) {};
          \node at (2.5, -.5) {\includegraphics[width=7mm]{db.png}};

          \node[bob,mirrored,minimum size=8mm] (bob) at (4, 1) {};
            \node at (4.5, .7) {\includegraphics[width=2mm]{phone.png}};
          \node[charlie,mirrored,minimum size=8mm] (charlie) at (4, 0) {};
            \node at (4.5, -.3) {\includegraphics[width=2mm]{phone.png}};
          \node[dave,mirrored,minimum size=8mm] (dave) at (4, -1) {};
            \node at (4.5, -1.3) {\includegraphics[width=2mm]{phone.png}};

          \draw[->,thick] (alice) -- (server);
          \draw[->,thick] (server) -- (bob);
          \draw[->,thick] (server) -- (charlie);
          \draw[->,thick] (server) -- (dave);
        \end{tikzpicture}

        Alice sends \emph{one} message, server distributes to the group.
    \end{column}\pause
    \begin{column}{.5\linewidth}
      \textbf{Client fan-out}

        \begin{tikzpicture}
          \node[alice,minimum size=1cm] (alice) at (0, 0) {};
          \node at (-1, -.4) {\includegraphics[width=3mm]{phone.png}};
          \node[guard,mirrored, minimum size=1cm] (server) at (2, 0) {};
          \node at (2.5, -.5) {\includegraphics[width=7mm]{db.png}};

          \node[bob,mirrored,minimum size=8mm] (bob) at (4, 1) {};
            \node at (4.5, .7) {\includegraphics[width=2mm]{phone.png}};
          \node[charlie,mirrored,minimum size=8mm] (charlie) at (4, 0) {};
            \node at (4.5, -.3) {\includegraphics[width=2mm]{phone.png}};
          \node[dave,mirrored,minimum size=8mm] (dave) at (4, -1) {};
            \node at (4.5, -1.3) {\includegraphics[width=2mm]{phone.png}};

          \draw[->,thick] ($(alice.east)$) -- ($(server.west)$);
          \draw[->,thick] ($(alice.east) + (0, .4)$) -- ($(server.west) + (0, .4)$);
          \draw[->,thick] ($(alice.east) + (0, -.4)$) -- ($(server.west) + (0, -.4)$);
          \draw[->,thick] (server) -- (bob);
          \draw[->,thick] (server) -- (charlie);
          \draw[->,thick] (server) -- (dave);
        \end{tikzpicture}

        Alice sends \emph{N} messages, server delivers to individuals.
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Minimizing metadata (2)}
  \framesubtitle{Group structure (contd.)\footnotemark}
  \footnotetext[2]{Private Groups on Signal's blog (2019): \url{https://signal.org/blog/signal-private-group-system/}}

  Signal chooses \emph{client fan-out}, for privacy.

  \bigskip

  \begin{columns}[t]
    \begin{column}{.5\linewidth}
      Non-obvious \emph{disadvantages}:
      \begin{itemize}
        \item Everyone is administrator
          \pause
        \item Group inconsistent on missed messages
      \end{itemize}
    \end{column}\pause
    \begin{column}{.5\linewidth}
      Solution, \emph{GroupV2}:
      \begin{itemize}
        \item Store \emph{encrypted group state} on server
          \pause
        \item \emph{Prove} necessary rights on modification\pause,\\
          \emph{without identification}!
          \pause
        \item \emph{Distribute} group structure via p2p messages\pause,\\
          but \emph{validate} against server signature.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Whisperfish}
  \framesubtitle{Signal on Sailfish~OS}

  \begin{columns}
    \begin{column}{.3\linewidth}
      Third-party client for Signal on \emph{Sailfish~OS}.
      \begin{center}
        \includegraphics[width=.6\linewidth]{harbour-whisperfish.png}
      \end{center}
      Stack:
        Qt\,5\footnote{Qt\,5.6, yes it's old.} with QML on Wayland
    \end{column}
    \begin{column}{.7\linewidth}
      \begin{description}
        \item[Apr 2016] Andrew E.\ Bruno starts Whisperfish in \emph{Go}
          \pause
        \item[Feb 2019] Andrew steps down
          \pause
        \item[Jul 2019] Ruben steps up: rewrite in C++ begins
          \pause
        \item[Nov 2019] Ruben has too many C++-headaches,
          switches to \emph{Rust}
          \pause
        \item[Apr 2020] Decision to keep the Rustification
          \pause
        \item[May 2020] First integration of Actix with Qt.
          \pause
        \item[2021-2022] Various ``beta'' releases.
      \end{description}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Whisperfish}
  \framesubtitle{Difficulties}

  \pause
  \begin{multicols}{2}
    \begin{itemize}
      \item Rust used to \emph{not exist} on Sailfish~OS%
        \footnote{\url{https://gitlab.com/whisperfish/whisperfish/-/merge_requests/179}}
        \pause
      \item Rust compiler on Sailfish~OS \emph{is outdated}%
        \footnote{\url{https://github.com/sailfishos/rust/pull/15}}
        \pause
      \item Qt is outdated%
        \footnote{\url{https://github.com/woboq/qmetaobject-rs/pull/196}}
        \pause
      \item Signal \emph{used to be ``hostile''} towards 3\textsuperscript{rd} party clients%
        \footnote{\url{https://github.com/LibreSignal/LibreSignal/issues/43\#issuecomment-269103563}}
        \pause
      \item Signal ``Moves Fast and Breaks Things''
        \pause
      \item Jolla ``Moves Fast and Breaks Things'', slightly slower, less often
        \pause
    \end{itemize}
  \end{multicols}

  Because of this: Whisperfish is \emph{not as privacy-friendly} as upstream Signal.
\end{frame}

\begin{frame}{Notes on alternatives}
\end{frame}

\begin{frame}{Notes on alternatives}
  \framesubtitle{Telegram}

  \begin{center}
    \includegraphics[width=.5\linewidth]{telegram.png}
  \end{center}
\end{frame}

\begin{frame}{Notes on alternatives}
  \framesubtitle{WhatsApp}

  \begin{center}
    \includegraphics[width=.7\linewidth]{whatsapp.png}
  \end{center}
\end{frame}

\begin{frame}{Notes on alternatives}
  \framesubtitle{Session}

  \begin{center}
    \includegraphics[width=.7\linewidth]{session.png}
  \end{center}
\end{frame}

\begin{frame}{Pointers}
  Written article about secure messaging applications: \url{https://www.rubdos.be/2022/04/15/about-secure-messengers.html}%
  \footnote{Dutch version:
    \url{https://cybersecurity-research.be/x/2967}
  }%
  \textsuperscript{,}%
  \footnote{Dutch article about cryptographic techniques for data minimisation:
    \url{https://cybersecurity-research.be/x/2810}
  }

  \bigskip

  Signal's blog:
  \url{https://signal.org/blog/}

  \bigskip

  Article about zero-knowledge proofs:
  \url{https://blog.cryptographyengineering.com/2014/11/27/zero-knowledge-proofs-illustrated-primer/}
\end{frame}

\end{document}
